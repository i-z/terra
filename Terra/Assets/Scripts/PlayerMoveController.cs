﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMoveController : MonoBehaviour
{
    const float HOR_POS_THRESHOLD = 60f;
    const float VER_POS_THRESHOLD = 35f;

    [SerializeField] float positionPitchFactor = -0.5f;
    [SerializeField] float positionYewFactor = 0.75f;
    [SerializeField] float positionRollFactor = 0.75f;

    [SerializeField] float controlPitchFactor = -10f;
    [SerializeField] float controlYewFactor = 20f;
    [SerializeField] float controlRollFactor = -20f;

    [Tooltip("In m/s")][SerializeField] float xSpeed = 50f;
    [Tooltip("In m/s")][SerializeField] float ySpeed = 50f;

    private float xThrow;
    private float yThrow;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.CalculateThrow();
        this.CalculatePosition();
        this.CalculateRotation();
    }

    private void CalculateThrow()
    {
        this.xThrow = CrossPlatformInputManager.GetAxis("Horizontal");
        this.yThrow = CrossPlatformInputManager.GetAxis("Vertical");
    }

    private void CalculatePosition()
    {
        float xOffset = this.xThrow * xSpeed * Time.deltaTime;
        float yOffset = this.yThrow * ySpeed * Time.deltaTime;

        float rawXPos = transform.localPosition.x + xOffset;
        float rawYPos = transform.localPosition.y + yOffset;

        float clampedXPos = Mathf.Clamp(rawXPos, -HOR_POS_THRESHOLD, HOR_POS_THRESHOLD);
        float clampedYPos = Mathf.Clamp(rawYPos, -VER_POS_THRESHOLD, VER_POS_THRESHOLD);

        this.transform.localPosition = new Vector3(clampedXPos, clampedYPos, this.transform.localPosition.z);
    }

    private void CalculateRotation()
    {
        float positionPitch = transform.localPosition.y * this.positionPitchFactor;
        float controlPitch = this.yThrow * this.controlPitchFactor;
        float pitch = positionPitch + controlPitch;

        float positionYew = transform.localPosition.x * this.positionYewFactor;
        float controlYew = this.xThrow * this.controlYewFactor;
        float yaw = positionYew + controlYew;

        float postionRoll = transform.localPosition.x * positionRollFactor;
        float controlRoll = this.xThrow * controlRollFactor;
        float roll = postionRoll + controlRoll;

        this.transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }
}