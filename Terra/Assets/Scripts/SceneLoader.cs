﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] float gameLoadDelay = 2f;
    // Start is called before the first frame update
    void Start()
    {
        Invoke(nameof(LoadFirstScene), this.gameLoadDelay);
    }

    void LoadFirstScene()
    {
        SceneManager.LoadScene(1);
    }
}
